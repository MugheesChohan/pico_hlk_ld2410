#include "ld2410.h"
#include <string.h>

static uart_inst_t *ld2410_uart;
static uint8_t resp_buf[LD2410_MAX_RESPONSE_LENGTH] = {0};
static uint32_t chars_rxed = 0;

bool ld2410_init(uart_inst_t *uart, uint baud_rate, uint tx_gpio, uint rx_gpio)
{
    ld2410_uart = uart;

    /* Set up our UART with the required baud rate. */
    uart_init(uart, baud_rate);

    /* Set the TX and RX pins by using the function select on the GPIO */
    gpio_set_function(tx_gpio, GPIO_FUNC_UART);
    gpio_set_function(rx_gpio, GPIO_FUNC_UART);

    return true;
}

bool ld2410_deinit(uart_inst_t *uart)
{
    ld2410_uart = NULL;

    /* Set up our UART with the required baud rate. */
    uart_deinit(uart);

    return true;
}

static uint8_t * ld2410_read_response()
{
    int frame_start = -1;
    uint32_t start_time;

    chars_rxed = 0;

    memset(resp_buf, 0, LD2410_MAX_RESPONSE_LENGTH);

    start_time = time_us_32();

    do
    {
        if (uart_is_readable(ld2410_uart)) 
        {
            resp_buf[chars_rxed] = uart_getc(ld2410_uart);

            chars_rxed++;

            if ((chars_rxed > 3) && (resp_buf[chars_rxed - 4] == 0xFD) && 
                                    (resp_buf[chars_rxed - 3] == 0xFC) && 
                                    (resp_buf[chars_rxed - 2] == 0xFB) && 
                                    (resp_buf[chars_rxed - 1] == 0xFA))
            {
                frame_start = chars_rxed - 4U;
            }
                        
            if ((chars_rxed > 3) && (frame_start >= 0) && (resp_buf[chars_rxed - 4] == 0x04) && 
                                                          (resp_buf[chars_rxed - 3] == 0x03) && 
                                                          (resp_buf[chars_rxed - 2] == 0x02) && 
                                                          (resp_buf[chars_rxed - 1] == 0x01)) 
            {
                return &resp_buf[frame_start];
            }
        }

    } while(((time_us_32() - start_time) <= LD2410_RESP_WAIT_TIMEOUT) && (chars_rxed < LD2410_MAX_RESPONSE_LENGTH));

    return NULL;
}

bool ld2410_read_target_data(LD2410_DATA_FRAME *target_data)
{
    int frame_start = -1;
    uint32_t start_time;

    chars_rxed = 0;

    memset(resp_buf, 0, LD2410_MAX_RESPONSE_LENGTH);

    uint8_t *buf = NULL;

    start_time = time_us_32();

    do
    {
        if (uart_is_readable(ld2410_uart)) 
        {
            resp_buf[chars_rxed] = uart_getc(ld2410_uart);

            chars_rxed++;

            if ((chars_rxed > 3) && (resp_buf[chars_rxed - 4] == 0xF4) && 
                                    (resp_buf[chars_rxed - 3] == 0xF3) && 
                                    (resp_buf[chars_rxed - 2] == 0xF2) && 
                                    (resp_buf[chars_rxed - 1] == 0xF1))
            {
                frame_start = chars_rxed - 4U;
            }

            if ((chars_rxed > 3) && (frame_start >= 0) && (resp_buf[chars_rxed - 4] == 0xF8) && 
                                                          (resp_buf[chars_rxed - 3] == 0xF7) && 
                                                          (resp_buf[chars_rxed - 2] == 0xF6) && 
                                                          (resp_buf[chars_rxed - 1] == 0xF5)) 
            {
                buf = &resp_buf[frame_start];
            }
        }

    } while ((((time_us_32() - start_time) <= LD2410_RESP_WAIT_TIMEOUT) && (buf == NULL)) && (chars_rxed < LD2410_MAX_RESPONSE_LENGTH));

    if (buf)
    {
        target_data->type = buf[6];
        target_data->state = buf[8];
        target_data->motion_target_distance = ((uint16_t) buf[10] << 8) | (uint16_t) buf[9];
        target_data->motion_target_energy = buf[11];
        target_data->static_target_distance = ((uint16_t) buf[13] << 8) | (uint16_t) buf[12];
        target_data->static_target_energy = buf[14];
        target_data->detection_distance = ((uint16_t) buf[16] << 8) | (uint16_t) buf[15];

        if (target_data->type == 1)
        {
            /* Additional Engineering Mode Data. */
            target_data->maximum_motion_gate = buf[17];
            target_data->maximum_static_gate = buf[18];
            target_data->motion_gate_energy[0] = buf[19];
            target_data->motion_gate_energy[1] = buf[20];
            target_data->motion_gate_energy[2] = buf[21];
            target_data->motion_gate_energy[3] = buf[22];
            target_data->motion_gate_energy[4] = buf[23];
            target_data->motion_gate_energy[5] = buf[24];
            target_data->motion_gate_energy[6] = buf[25];
            target_data->motion_gate_energy[7] = buf[26];
            target_data->motion_gate_energy[8] = buf[27];
            target_data->static_gate_energy[0] = buf[28];
            target_data->static_gate_energy[1] = buf[29];
            target_data->static_gate_energy[2] = buf[30];
            target_data->static_gate_energy[3] = buf[31];
            target_data->static_gate_energy[4] = buf[32];
            target_data->static_gate_energy[5] = buf[33];
            target_data->static_gate_energy[6] = buf[34];
            target_data->static_gate_energy[7] = buf[35];
            target_data->static_gate_energy[8] = buf[36];
        }

        return true;
    }

    return false;
}

bool ld2410_enable_configuration()
{
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x04, 0x00, 0xFF, 0x00, 0x01, 0x00, 0x04, 0x03, 0x02, 0x01};

    uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 14);

    resp = ld2410_read_response();
    if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
    {
        return true;
    }

    return false;
}

bool ld2410_end_configuration()
{
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x02, 0x00, 0xFE, 0x00, 0x04, 0x03, 0x02, 0x01};

    uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 12);

    resp = ld2410_read_response();
    if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
    {
        return true;
    }

    return false;
}

bool ld2410_set_max_distance_duration(uint32_t moving, uint32_t stationary, uint32_t no_one_duration)
{
    bool pass = false;
    uint8_t *resp;
    uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA,
                     0x14, 0x00, 0x60, 0x00, 
                     0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                     0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
                     0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
                     0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        cmd[10] = (moving & 0x000000ff);
        cmd[11] = (moving & 0x0000ff00) >> 8;
        cmd[12] = (moving & 0x00ff0000) >> 16;
        cmd[13] = (moving & 0xff000000) >> 24;

        cmd[16] = (stationary & 0x000000ff);
        cmd[17] = (stationary & 0x0000ff00) >> 8;
        cmd[18] = (stationary & 0x00ff0000) >> 16;
        cmd[19] = (stationary & 0xff000000) >> 24;

        cmd[22] = (no_one_duration & 0x000000ff);
        cmd[23] = (no_one_duration & 0x0000ff00) >> 8;
        cmd[24] = (no_one_duration & 0x00ff0000) >> 16;
        cmd[25] = (no_one_duration & 0xff000000) >> 24;

        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 30);

        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}

bool ld2410_read_parameters(LD2410_PARAMS *params)
{
    bool pass = false;
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x61, 0x00, 0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 10);

        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            params->max_range_gate = resp[11];
            params->max_moving_gate = resp[12];
            params->max_stationary_gate = resp[13];
            params->motion_sensitivity[0] = resp[14];
            params->motion_sensitivity[1] = resp[15];
            params->motion_sensitivity[2] = resp[16];
            params->motion_sensitivity[3] = resp[17];
            params->motion_sensitivity[4] = resp[18];
            params->motion_sensitivity[5] = resp[19];
            params->motion_sensitivity[6] = resp[20];
            params->motion_sensitivity[7] = resp[21];
            params->motion_sensitivity[8] = resp[22];
            params->static_sensitivity[0] = resp[23];
            params->static_sensitivity[1] = resp[24];
            params->static_sensitivity[2] = resp[25];
            params->static_sensitivity[3] = resp[26];
            params->static_sensitivity[4] = resp[27];
            params->static_sensitivity[5] = resp[28];
            params->static_sensitivity[6] = resp[29];
            params->static_sensitivity[7] = resp[30];
            params->static_sensitivity[8] = resp[31];
            params->no_one_duration = ((uint16_t) resp[33] << 8) | (uint16_t) resp[32];

            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}

bool ld2410_enable_engineering_mode()
{
    bool pass = false;
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x02, 0x00, 0x62, 0x00, 0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 12);

        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}

bool ld2410_close_engineering_mode()
{
    bool pass = false;
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x02, 0x00, 0x63, 0x00, 0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 12);

        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}

bool ld2410_set_gate_sensitivity(uint32_t gate, uint32_t moving, uint32_t stationary)
{
    bool pass = false;
    uint8_t *resp;
    uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA,
                     0x14, 0x00, 0x64, 0x00,
                     0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                     0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
                     0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
                     0x04, 0x03, 0x02, 0x01, 
                     0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        cmd[10] = (gate & 0x000000ff);
        cmd[11] = (gate & 0x0000ff00) >> 8;
        cmd[12] = (gate & 0x00ff0000) >> 16;
        cmd[13] = (gate & 0xff000000) >> 24;

        cmd[16] = (moving & 0x000000ff);
        cmd[17] = (moving & 0x0000ff00) >> 8;
        cmd[18] = (moving & 0x00ff0000) >> 16;
        cmd[19] = (moving & 0xff000000) >> 24;

        cmd[22] = (stationary & 0x000000ff);
        cmd[23] = (stationary & 0x0000ff00) >> 8;
        cmd[24] = (stationary & 0x00ff0000) >> 16;
        cmd[25] = (stationary & 0xff000000) >> 24;

        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 30);

        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}

bool ld2410_read_firmware_version(uint16_t *firmware_type, uint16_t *major_version, uint32_t *minor_version)
{
    bool pass = false;
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x02, 0x00, 0xA0, 0x00, 0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 12);
        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            *firmware_type = ((uint16_t) resp[11] << 8) | (uint16_t) resp[10];
            *major_version = ((uint16_t) resp[13] << 8) | (uint16_t) resp[12];
            *minor_version = ((uint32_t) resp[17] << 24) | ((uint32_t) resp[16] << 16) | 
                             ((uint32_t) resp[15] << 8) | (uint32_t) resp[14];

            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}

bool ld2410_set_baud_rate(uint16_t baud_rate_parameter)
{
    bool pass = false;
    uint8_t *resp;

    if ((baud_rate_parameter > 0x0) && (baud_rate_parameter < 0x9))
    {
        uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x04, 0x00, 0xA1, 0x00, 0x00, 0x00, 0x04, 0x03, 0x02, 0x01};

        if (ld2410_enable_configuration() == true)
        {
            cmd[8] = (uint8_t) (baud_rate_parameter & 0xFF);
            cmd[9] = (uint8_t) (baud_rate_parameter >> 8);

            uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 14);

            resp = ld2410_read_response();
            if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
            {
                pass = true;
            }

            ld2410_end_configuration();
        }
    }
    
    return pass;
}

bool ld2410_factory_reset()
{
    bool pass = false;
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x02, 0x00, 0xA2, 0x00, 0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 12);

        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}

bool ld2410_restart_module()
{
    bool pass = false;
    uint8_t *resp;
    const uint8_t cmd[] = {0xFD, 0xFC, 0xFB, 0xFA, 0x02, 0x00, 0xA3, 0x00, 0x04, 0x03, 0x02, 0x01};

    if (ld2410_enable_configuration() == true)
    {
        uart_write_blocking(ld2410_uart, (const uint8_t *) cmd, 12);

        resp = ld2410_read_response();
        if ((resp) && (resp[8] == 0U) && (resp[9] == 0U))
        {
            pass = true;
        }

        ld2410_end_configuration();
    }

    return pass;
}
