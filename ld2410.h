#include "pico/stdlib.h"
#include "hardware/uart.h"

#define LD2410_MAX_CMD_LENGTH         32
#define LD2410_MAX_RESPONSE_LENGTH    128
#define LD2410_RESP_WAIT_TIMEOUT      1000000

#define LD2410_DATA_BITS 8
#define LD2410_STOP_BITS 1
#define LD2410_PARITY    UART_PARITY_NONE

typedef struct _ld2410_params
{
    uint8_t max_range_gate;
    uint8_t max_moving_gate;
    uint16_t no_one_duration;
    uint8_t max_stationary_gate;
    uint8_t motion_sensitivity[9];
    uint8_t static_sensitivity[9];

} LD2410_PARAMS;

typedef struct _ld2410_data_frame
{
    uint16_t motion_target_distance;
    uint16_t static_target_distance;
    uint16_t detection_distance;
    uint8_t type;
    uint8_t state;
    uint8_t motion_target_energy;
    uint8_t static_target_energy;

    /* Additional Engineering Mode Data. */
    uint8_t maximum_motion_gate;
    uint8_t maximum_static_gate;
    uint8_t motion_gate_energy[9];
    uint8_t static_gate_energy[9];

} LD2410_DATA_FRAME;

bool ld2410_init(uart_inst_t *uart, uint baud_rate, uint tx_gpio, uint rx_gpio);
bool ld2410_deinit(uart_inst_t *uart);
bool ld2410_read_target_data(LD2410_DATA_FRAME *target_data);
bool ld2410_set_max_distance_duration(uint32_t moving, uint32_t stationary, uint32_t no_one_duration);
bool ld2410_read_parameters(LD2410_PARAMS *params);
bool ld2410_enable_engineering_mode();
bool ld2410_close_engineering_mode();
bool ld2410_set_gate_sensitivity(uint32_t gate, uint32_t moving, uint32_t stationary);
bool ld2410_read_firmware_version(uint16_t *firmware_type, uint16_t *major_version, uint32_t *minor_version);
bool ld2410_set_baud_rate(uint16_t baud_rate);
bool ld2410_factory_reset();
bool ld2410_restart_module();