*************************************************** 
* Driver for HLK-LD2410 for Raspberry PI Pico SDK *
***************************************************

The HLK-LD2410 modules can detect targets that are either moving called "moving" or "motion" targets. And targets 
that are not moving called "static" or stationary targets. The module also defines range bins or range gates or 
distance gates or simply called gates that are typically numbered from 0 to 8. Each gate corresponds to a range of 
0.75 meters. For example if maximum range gate is set to 2, it means that a target within a range of (2 * 0.75m) = 1.5m
can be detected. There are three types of parameters that can be set:

1. Maximum Detection distance - This is the maximum distance at which targets can be detected and is set in units of range gates.
2. Sensitivity - Sensitivity is the energy value ranging from 0 - 100 that can be be set for each range gate. A target will only be
                 detected when the target energy value is greater than the sensitivity value set for that range gate.
3. No one duration - This is the time in seconds for which a target is still reported as present even after it has exited the radar detection range.

Driver Usage:

    To use this driver, just copy the "ld2410.c" and "ld2410.h" files in any of your pico project and update the CMakeLists.txt file accordingly.
    The file "ld2410_example.c" contains a simple demonstration for the module.

******************************
* Important Data Structures  *
******************************

-------------
LD2410_PARAMS
-------------

Description:

    This structure is used to read currently programmed parameters of the radar using the ld2410_read_parameters() API.

Definition:

    typedef struct _ld2410_params
    {
        uint8_t max_range_gate;
        uint8_t max_moving_gate;
        uint16_t no_one_duration;
        uint8_t max_stationary_gate;
        uint8_t motion_sensitivity[9];
        uint8_t static_sensitivity[9];

    } LD2410_PARAMS;

Elements:

    max_range_gate - Maximum range gate. 8 usually.
    max_moving_gate - Configured maximum detection range in units of range gates for moving targets. Values ranging from 2 to 8.
    no_one_duration - Configured value of no one duration.
    max_stationary_gate - Configured maximum detection range in units of range gates for stationary targets. Values ranging from 2 to 8
    motion_sensitivity - This is an array containing configured sensitivity of moving targets for all range gates. Value ranges from 0 to 100.
    static_sensitivity - This is an array containing configured sensitivity of stationary targets for all range gates. Value ranges from 0 to 100.

-----------------
LD2410_DATA_FRAME
-----------------

Description:

    This structure is used to read target data information using the ld2410_read_target_data() API.

Definition:

    typedef struct _ld2410_data_frame
    {
        uint16_t motion_target_distance;
        uint16_t static_target_distance;
        uint16_t detection_distance;
        uint8_t type;
        uint8_t state;
        uint8_t motion_target_energy;
        uint8_t static_target_energy;

        /* Additional Engineering Mode Data. */
        uint8_t maximum_motion_gate;
        uint8_t maximum_static_gate;
        uint8_t motion_gate_energy[9];
        uint8_t static_gate_energy[9];

} LD2410_DATA_FRAME;

Elements:

    motion_target_distance - Moving target distance in cm.
    static_target_distance - Stationary target distance in cm.
    detection_distance - Detection distance of the target in cm.
    type - Type of target information. 0x01 = Engineering Mode Data, 0x02 = Normal Mode Data.
    state - Target State. 0x00 = No Target, 0x01 = Moving Target, 0x02 = Stationary Target, 0x03 = Moving & Stationary Target.
    motion_target_energy - Energy of moving target. Value ranging from 0 to 100.
    static_target_energy - Energy of stationary target. Value ranging from 0 to 100. 
    maximum_motion_gate - Maximum rage gate for moving targets. Usually 8. Only valid in engineering mode.
    maximum_static_gate - Maximum rage gate for stationary targets. Usually 8. Only valid in engineering mode.
    motion_gate_energy - Array containing energy of moving targets in each range gate. Value ranging from 0 to 100. Only valid in engineering mode.
    static_gate_energy - Array containing energy of stationary targets in each range gate. Value ranging from 0 to 100. Only valid in engineering mode.

**********************
* API Documentation  *
**********************

-----------
ld2410_init 
-----------

Prototype:

    bool ld2410_init(uart_inst_t *uart, uint baud_rate, uint tx_gpio, uint rx_gpio);

Parameters:

    uart - UART instance connected with LD2410 module
    baud_rate - Baud rate of uart used for communication
    tx_gpio - GPIO number of uart's TX pin
    rx_gpio - GPIO number of uart's RX pin

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function sets up uart pins, baudrate and initializes the uart for communicating with LD2410 module.

-------------
ld2410_deinit
-------------

Prototype:

    bool ld2410_deinit(uart_inst_t *uart);

Parameters:

    uart - UART instance connected with LD2410 module

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function de-initializes the uart to stop communication with LD2410 module.

-----------------------
ld2410_read_target_data
-----------------------

Prototype:

    bool ld2410_read_target_data(LD2410_DATA_FRAME *target_data);

Parameters:

    target_data - Pointer to LD2410_DATA_FRAME structure to store retrieved target data from module.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function retrieves target data information from the module. This target data information is stored in
    a structure of type LD2410_DATA_FRAME, pointer to which is passed as a parameter to this function. Refer to 
    the description of LD2410_DATA_FRAME data structure for details.

---------------------------------
ld2410_set_max_distance_duration
---------------------------------

Prototype:

    bool ld2410_set_max_distance_duration(uint32_t moving, uint32_t stationary, uint32_t no_one_duration);

Parameters:

    moving - Maximum detection range in units of range gates for moving targets. Values ranging from 2 - 8.
    stationary - Maximum detection range in units of range gates for stationary targets. Values ranging from 2 - 8.
    no_one_duration - No one duration in number of seconds. Maximum value 0xFFFF.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function sets maximum detection range for moving and stationary targets, and the no one duration.
    Detection range is set in units of range gates ranging from 2 - 8. No one duration is set in units of
    seconds.

-----------------------
ld2410_read_parameters
-----------------------

Prototype:

    bool ld2410_read_parameters(LD2410_PARAMS *params);

Parameters:

    params - Pointer to LD2410_PARAMS structure to store currently programmed parameters in the module.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function reads the current values of configuration parameters programmed in the LD2410 module. The 
    read values are stored in a structure of type LD2410_PARAMS, pointer to which is passed as a parameter 
    to this function. Refer to the description of LD2410_PARAMS data structure for details.

-------------------------------
ld2410_enable_engineering_mode
-------------------------------

Prototype:

    bool ld2410_enable_engineering_mode();

Parameters:

    None.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function turns on the engineering mode in the module. In engineering mode, the module reports
    extra target data information. Refer to the description of LD2410_DATA_FRAME data structure for details.

------------------------------
ld2410_close_engineering_mode
------------------------------

Prototype:

    bool ld2410_close_engineering_mode();

Parameters:

    None.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function turns off the engineering mode in the module.

----------------------------
ld2410_set_gate_sensitivity
----------------------------

Prototype:
    
    bool ld2410_set_gate_sensitivity(uint32_t gate, uint32_t moving, uint32_t stationary);

Parameters:

    gate - The range gate number for which sensitivity is to be adjusted, with values ranging from 0 to 8.
    moving - Sensitivity of moving targets. Value ranges from 0 to 100.
    stationary - Sensitivity of stationary targets. Value ranges from 0 to 100.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function sets the distance gate sensitivities. The configuration persists even in the event of a power failure. 
    It supports both individual adjustments for each distance gate and the concurrent configuration of all gates to a unified value. 
    If you choose to set the sensitivity of all distance gates simultaneously, configure the distance gate value as 0xFFFF.

-----------------------------
ld2410_read_firmware_version
-----------------------------

Prototype:

    bool ld2410_read_firmware_version(uint16_t *firmware_type, uint16_t *major_version, uint32_t *minor_version);

Parameters:

    firmware_type - Firmware type
    major_version - Major version number
    minor_version - Minor version number

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function reads the firmware version information.

--------------------
ld2410_set_baud_rate
--------------------

Prototype:

    bool ld2410_set_baud_rate(uint16_t baud_rate);

Parameters:

    baud_rate - Baud rate to set for uart communication in the module.
                The factory default value is 0x0007, which is 256000.
               
                baud_rate value     Actual Programmed Baud Rate

                0x0001              9600
                0x0002              19200
                0x0003              38400
                0x0004              57600
                0x0005              115200
                0x0006              230400
                0x0007              256000
                0x0008              460800

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function sets the baud rate of the uart port of the LD2410 module. The baud rate change will
    take effect after restarting the module using ld2410_restart_module() API. Make sure you call ld2410_init()
    API with the new baud rate value after restarting the module, to update the uart baud rate at the rp2040 
    micro-controller side too.

--------------------
ld2410_factory_reset
--------------------

Prototype:

    bool ld2410_factory_reset();

Parameters:

    None.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function performs a factory reset of the LD2410 module. This will restore all configuration values to 
    their original values. The configuration values take effect after restarting the module.

---------------------
ld2410_restart_module
---------------------

Prototype:

    bool ld2410_restart_module();

Parameters:

    None.

Return Value:

    true - Successful operation
    false - Unsuccessful operation

Description:

    This function restarts the LD2410 module.
