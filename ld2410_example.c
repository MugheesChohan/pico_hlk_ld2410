/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "ld2410.h"

LD2410_DATA_FRAME target_data_frame;

int main() {

    uint16_t firmware_type = 0U;
    uint16_t major_version = 0U;
    uint32_t minor_version = 0U;
    
    stdio_init_all();

    sleep_ms(5000);

    printf("Hello, LD2410 world!");  

    ld2410_init(uart0, 256000, 0 /* UART0 TX Pin */, 1 /* UART0 RX Pin */);

    ld2410_read_firmware_version(&firmware_type, &major_version, &minor_version);

    ld2410_enable_engineering_mode();

    while (1)
    {
        uint8_t *buf;

        (void) ld2410_read_target_data(&target_data_frame);

        /* Clear screen. */
        printf("%c%c%c%c", 0x1B, 0x5B, 0x32, 0x4A);

        printf("Firmware Type = %d \n\r", firmware_type);
        printf("Major Version = %d \n\r", major_version);
        printf("Minor_version = %d \n\r", minor_version);

        printf("Type = %d \n\r", target_data_frame.type);
        printf("State = %d \n\r", target_data_frame.state);
        printf("Motion target distance = %d cm\n\r", target_data_frame.motion_target_distance);
        printf("Motion target energy = %d \n\r", target_data_frame.motion_target_energy);
        printf("Static target distance = %d cm\n\r", target_data_frame.static_target_distance);
        printf("Static target energy = %d \n\r", target_data_frame.static_target_energy);
        printf("Detection distance = %d cm\n\r", target_data_frame.detection_distance);

        if (target_data_frame.type == 1)
        {
            printf("Maximum Motion Gate = %d \n\r", target_data_frame.maximum_motion_gate);
            printf("Maximum Static Gate = %d \n\r", target_data_frame.maximum_static_gate);

            printf("Motion Gate Energies = %d %d %d %d %d %d %d %d %d \n\r", target_data_frame.motion_gate_energy[0], target_data_frame.motion_gate_energy[1],
                                                                             target_data_frame.motion_gate_energy[2], target_data_frame.motion_gate_energy[3],
                                                                             target_data_frame.motion_gate_energy[4], target_data_frame.motion_gate_energy[5],
                                                                             target_data_frame.motion_gate_energy[6], target_data_frame.motion_gate_energy[7],
                                                                             target_data_frame.motion_gate_energy[8]);

            printf("Static Gate Energies = %d %d %d %d %d %d %d %d %d \n\r", target_data_frame.static_gate_energy[0], target_data_frame.static_gate_energy[1],
                                                                             target_data_frame.static_gate_energy[2], target_data_frame.static_gate_energy[3],
                                                                             target_data_frame.static_gate_energy[4], target_data_frame.static_gate_energy[5],
                                                                             target_data_frame.static_gate_energy[6], target_data_frame.static_gate_energy[7],
                                                                             target_data_frame.static_gate_energy[8]);
        }

        printf("\n\r\n\r");

        sleep_ms(100);
    }
}
